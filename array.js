// //soal no 1
function range(startNum,finishNum){
var rangeArray = [];
if(startNum == undefined || finishNum == undefined){
    return -1;
}
else if (startNum <finishNum){
for(i = startNum; i<=finishNum;i++)
{
    rangeArray.push(i);
}
}
else {
    for(j=finishNum;j<=startNum;j++){
        rangeArray.push(j);
        rangeArray.sort(function(startNum,finishNum){
            return finishNum - startNum
        });
    }
}
return rangeArray;
}

console.log(range(1, 10)) 
console.log(range(1)) 
console.log(range(11,18)) 
console.log(range(54, 50)) 
console.log(range()) 

// //soal no 2
function rangeWithStep(angka1,angka2,step){
    var arrRange2 = [];
    if(angka1<angka2){
        for(i =angka1;i<=angka2;i+=step){
            arrRange2.push(i);
        }
        return arrRange2;
    }
    else if (angka1 > angka2){
        for(var j=angka1;j>=angka2;j-=step){
            arrRange2.push(j);
        }
        return arrRange2;
    }
    else {
        return -1 
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// //soal no 3

function sum (first,last,how){
    var show=0;
    var arr;
    var i;
    if (!first && !last && !how){
        show=0;
    }
    else if(first && !last && !how){
        show=first;
    }
    else {
        if (!how){
            arr = rangeWithStep(first,last,1)
        }
        else {
            arr = rangeWithStep(first,last,how)

        }
        for (i=0;i<=arr.length;i++){
            show = show + parseInt(arr[i]);
        }
    }
    return show;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// //soal no 4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(arr) {
    var i

    for (i = 0; i < arr.length; i++) {
        console.log(`Nomor ID: ${arr[i][0]}`);
        console.log(`Nama Lengkap: ${arr[i][1]}`);
        console.log(`TTL: ${arr[i][2]} ${arr[i][3]}`);
        console.log(`Hobi: ${arr[i][4]}`);
        console.log();
    }
}

dataHandling(input);



// SOAL 5

function balikKata(input) {
    var i
    var output = ''

    for (i = input.length - 1; i >= 0; i--) {
        output = output + input[i]
    }

    return output;
}

console.log(balikKata("Kasur Rusak")) ;// kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")) ;// racecar
console.log(balikKata("I am Sanbers")) ;// srebnaS ma I 

//soal no 6
function dataHandling2(arr) {
    var tanggal = []
    var bulan
    var pBulan
    var stringNama


    // Splice
    arr.splice(1, 1, `${arr[1]} Elsharawy`)
    arr.splice(2, 1, `Provinsi ${arr[2]}`)
    arr.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(arr)


    // Split
    tanggal = arr[3].split("/")
    bulan = parseInt(tanggal[1])
    switch (bulan) {
        case 1: {
            pBulan = "Januari";
            break;
        }
        case 2: {
            pBulan = "Februari";
            break;
        }
        case 3: {
            pBulan = "Maret";
            break;
        }
        case 4: {
            pBulan = "April";
            break;
        }
        case 5: {
            pBulan = "Mei";
            break;
        }
        case 6: {
            pBulan = "Juni";
            break;
        }
        case 7: {
            pBulan = "Juli";
            break;
        }
        case 8: {
            pBulan = "Agustus";
            break;
        }
        case 9: {
            pBulan = "September";
            break;
        }
        case 10: {
            pBulan = "Oktober";
            break;
        }
        case 11: {
            pBulan = "November";
            break;
        }
        case 12: {
            pBulan = "Desember";
            break;
        }
        default: {
            pBulan = "";
        }
    }
    console.log(parseBulan)

    // Sorting
    tanggal.sort(
        function (a, b) {
            return b - a
        }
    )
    console.log(tanggal)

    // Join
    tanggal = (arr[3].split("/")).join("-")
    console.log(tanggal)
    
    // Slice
    stringNama = String(arr[1])
    console.log(stringNama.slice(0,15))
}

input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input)

