// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var time = 10000
function count(bil){
    if (bil==books.length){
        return 0
    }else{
        readBooks(time,books[bil],function(check){
            time=books[bil].timeSpent
            count(bil+1)
        })
    }
}
count(0)