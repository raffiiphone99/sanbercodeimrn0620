// //soal no 1 TEST CASES Bandingkan Angka
// function bandingkan(num1,num2){
//     if(num1==num2 || num1 < 0 || num2 <0){
//         return -1;
//     }
//     else if(num1<num2){
//         return num2;
//     }
//     else if(num1>num2){
//         return num1;
//     }   
//     else if(num1 > 0 && !num2 ){
//         return num1;
//     }
//     else if(num2 > 0 && !num1 ){
//         return num2;
//     }
// }
// console.log(bandingkan(10, 15)); // 15
// console.log(bandingkan(12, 12)); // -1
// console.log(bandingkan(-1, 10)); // -1 
// console.log(bandingkan(112, 121));// 121
// console.log(bandingkan(1)); // 1
// console.log(bandingkan()); // -1
// console.log(bandingkan("15", "18")) // 18

// //no 2 BalikString
// function balikString(input) {
//     var i
//     var output = ''

//     for (i = input.length - 1; i >= 0; i--) {
//         output = output + input[i]
//     }

//     return output;
// }
// console.log(balikString("abcde")) // edcba
// console.log(balikString("rusak")) // kasur
// console.log(balikString("racecar")) // racecar
// console.log(balikString("haji")) // ijah

// // no 3 Palindrome

// function palindrome(input) {
//     var i
//     var output = ''

//     for (i = input.length - 1; i >= 0; i--) {
//         output = output + input[i]
//     }
//     if (input == output){
//         return true;
//     }
//     else if (input != output){
//         return false;
//     }
// }
// console.log(palindrome("kasur rusak")) // true
// console.log(palindrome("haji ijah")) // true
// console.log(palindrome("nabasan")) // false
// console.log(palindrome("nababan")) // true
// console.log(palindrome("jakarta")) // false